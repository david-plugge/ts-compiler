import { EventEmitter } from 'events';
import { Handler, AstNode, Token } from '.';
import { Context } from './context';

export class Parser extends EventEmitter {
    private readonly rules: Array<{ result: string; parts: string[]; handler: Handler }> = [];
    private queue: Array<Token | { type: string; value: AstNode }> = [];

    constructor() {
        super();
        this.on('pipe', () => {
            this.queue = [];
        });
    }

    public production(rule: string, handler: Handler) {
        const [result, parts] = rule.split(':', 2);

        this.rules.unshift({
            result: result.trim(),
            parts: parts
                .split(' ')
                .filter(Boolean)
                .map((p) => p.trim()),
            handler,
        });
        return this;
    }

    public write(token: Token) {
        this.queue.push(token);
    }
    public get ast(): AstNode {
        return this.queue[0].value as any;
    }
    public end() {
        this._generateAst();
        this.emit('end');
    }

    public compile() {
        const ast = this.ast;
        const ctx = new Context();
        ast.compile(ctx);
        return { commands: ctx.commands, consts: ctx.consts };
    }

    private _generateAst() {
        const { queue, rules } = this;
        const rulesLength = rules.length;

        for (let i = 0; i < rulesLength; i++) {
            const { parts, result, handler } = rules[i];
            let correct = 0;
            for (let j = 0; j < queue.length; j++) {
                if (queue[j].type === parts[correct]) correct++;
                else correct = 0;

                if (correct === parts.length) {
                    const pos = j - correct + 1;
                    const slice = queue.splice(pos, correct);

                    const value = handler(...slice.map((s) => s.value));
                    queue.splice(pos, 0, { type: result, value });

                    i = -1;
                    break;
                }
            }
        }
    }
}
