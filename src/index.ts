import { Context } from './context';

export { Lexer } from './lexer';

export type Token = { type: string; value: string; pos: { row: number; col: number } };
export type Handler = (...s: any[]) => AstNode;

export abstract class AstNode {
    abstract compile(ctx: Context): any;
}
