export class Context<T = any> {
    private readonly _commands: Array<any> = [];
    private readonly _consts: any[] = [];

    public emit(command: T, ...args: any[]) {
        this._commands.push(command, ...args);
    }

    public get commands() {
        return this._commands;
    }
    public get consts() {
        return this._consts;
    }

    public new_const(c: any) {
        const index = this._consts.indexOf(c);
        return index >= 0 ? index : this._consts.push(c) - 1;
    }
}
