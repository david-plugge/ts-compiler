import { EventEmitter } from 'events';
import { Token } from '.';
import { Parser } from './parser';

export declare interface Lexer {
    on(event: 'token', listener: (data: Token) => void): this;
    on(event: 'end', listener: () => void): this;
    on(event: string, listener: (...args: any[]) => void): this;
}

export class Lexer extends EventEmitter {
    public writable = true;
    private index = 0;
    private row = 0;
    private col = 0;
    private re!: RegExp;
    private rules: Array<{ name: string; symbol?: any; rules: string }> = [];
    private changed = false;

    constructor() {
        super();
        this.on('pipe', () => {
            this.index = 0;
            this.row = 0;
            this.col = 0;
            if (this.changed) {
                this.re = new RegExp(`(?:${this.rules.map((r) => `(?:${r.rules})`).join('|')})`, 'gm');
            }
        });
    }

    public add(name: string, ...rules: RegExp[]): this;
    public add(name: string, symbol: string, ...rules: RegExp[]): this;
    public add(name: string, symbol: string | RegExp | null, ...rules: RegExp[]): this {
        this.changed = true;
        if (symbol instanceof RegExp) {
            rules.unshift(symbol);
            symbol = null;
        }
        const r = `((?:${rules.map((r) => `(?:${r.source})`).join('|')}))`;
        this.rules.push({ name, symbol, rules: r });
        return this;
    }

    public pipe(parser: Parser) {
        parser.emit('pipe', this);
        const handle = (token: Token) => parser.write(token);
        this.on('token', handle);
        this.once('end', () => {
            this.off('token', handle);
            parser.end();
        });
        return parser;
    }

    public parse(text: string) {
        this.write(text);
        return this;
    }

    public write(buffer: string | Uint8Array, cb?: (err?: Error) => void): boolean;
    public write(str: string | Uint8Array, encoding?: BufferEncoding, cb?: (err?: Error) => void): boolean;
    public write(chunk: Buffer, encoding?: BufferEncoding | ((err?: Error) => void), cb?: (err?: Error) => void) {
        if (typeof encoding === 'function') {
            encoding = undefined;
            cb = encoding;
        }
        const str = chunk.toString();

        const found = str.matchAll(this.re);

        for (const m of found) {
            const group = this.rules[m.lastIndexOf(m[0]) - 1];
            if (!group) throw new Error(`Cant find group name for ${m[0]}`);

            this.emit('token', { type: group?.name, value: m[0], pos: { row: this.row, col: this.col } });

            this.index = m.index! + m[0].length;
            const newLines = m[0].split('\n').length - 1;
            if (newLines) {
                this.col += newLines;
                this.row = m[0].length - m[0].lastIndexOf('\n');
            } else {
                this.row += m[0].length;
            }
        }

        if (this.index < str.length) {
            console.log('Not finished, wait for new chunk');
        }
        return true;
    }
    public end() {
        this.emit('end');
    }
}
