import { Lexer, AstNode } from '../src';
import { createReadStream } from 'fs';
import { Parser } from '../src/parser';
import * as ast from './ast';
import { Interpreter } from './interpreter';

const text = createReadStream('test.txt', { encoding: 'utf-8' });
const lexer = new Lexer();
const parser = new Parser();

lexer.add('BOOLEAN', /true/, /false/);
lexer.add('STRING', /".*?"/, /'.*?'/);
lexer.add('NUMBER', /[\.0-9]+/);
lexer.add('OPERATION', /[\+\-\*\/]/);
lexer.add('LPAREN', /\(/);
lexer.add('RPAREN', /\)/);
lexer.add('WS', /[ \t]/);
lexer.add('NL', /\n/);

parser.production('main : statements', (s) => s);
parser.production('statements : statement', (s) => new ast.Block([s]));
parser.production('statements : statements statement', (ss, s) => new ast.Block([...ss.statements, s]));
parser.production('statement : expr NL', (s) => new ast.Statement(s));
parser.production('expr : LPAREN expr RPAREN', (_, s: AstNode) => s);
parser.production('expr : expr _ OPERATION _ expr', (left: AstNode, _1, op: string, _2, right: AstNode) => new ast.BinaryOperation(op, left, right));
parser.production('expr : NUMBER', (s: string) => new ast.Number(parseInt(s)));
parser.production('expr : STRING', (s: string) => new ast.String(s));
parser.production('expr : BOOLEAN', (s: string) => new ast.Boolean(s === 'true'));
parser.production('_ : _ _', (a, b) => a + b);
parser.production('_ : WS', (s) => s);

text.pipe(lexer)
    .pipe(parser)
    .on('end', () => {
        const ctx = parser.compile();
        const i = new Interpreter(ctx.commands, ctx.consts);
        i.interpret();
    });
