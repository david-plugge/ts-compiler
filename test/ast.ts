import { AstNode } from '../src';
import { Context } from '../src/context';

export enum Command {
    POP_TOP,
    LOAD_CONST,
    BINARY_ADD,
    BINARY_MINUS,
    BINARY_MULTIPLY,
    BINARY_DIVIDE,
    UNARY_NEG,
    UNARY_NOT,
}

type Ctx = Context<Command>;

export class Number extends AstNode {
    private readonly value: number;

    constructor(value: number) {
        super();
        this.value = value;
    }
    compile(ctx: Ctx) {
        ctx.emit(Command.LOAD_CONST, ctx.new_const(this.value));
    }
}

export class Boolean extends AstNode {
    private readonly value: boolean;

    constructor(value: boolean) {
        super();
        this.value = value;
    }
    compile(ctx: Ctx) {
        ctx.emit(Command.LOAD_CONST, ctx.new_const(this.value));
    }
}

export class String extends AstNode {
    private readonly value: string;

    constructor(value: string) {
        super();
        this.value = value;
    }
    compile(ctx: Ctx) {
        ctx.emit(Command.LOAD_CONST, ctx.new_const(this.value));
    }
}

export class BinaryOperation extends AstNode {
    private readonly left: AstNode;
    private readonly right: AstNode;
    private readonly op: string;

    constructor(op: string, left: AstNode, right: AstNode) {
        super();
        this.op = op;
        this.left = left;
        this.right = right;
    }
    compile(ctx: Ctx) {
        this.left.compile(ctx);
        this.right.compile(ctx);
        const operations: Record<string, Command> = {
            '+': Command.BINARY_ADD,
            '-': Command.BINARY_MINUS,
            '*': Command.BINARY_MULTIPLY,
            '/': Command.BINARY_DIVIDE,
        };
        ctx.emit(operations[this.op]);
    }
}
export class UnaryOperation extends AstNode {
    private readonly node: AstNode;
    private readonly op: string;

    constructor(op: string, node: AstNode) {
        super();
        this.op = op;
        this.node = node;
    }
    compile(ctx: Ctx) {
        this.node.compile(ctx);
        const operations: Record<string, Command> = {
            '-': Command.UNARY_NEG,
            '!': Command.UNARY_NOT,
        };
        ctx.emit(operations[this.op]);
    }
}

export class Statement extends AstNode {
    private readonly expr: AstNode;

    constructor(expr: AstNode) {
        super();
        this.expr = expr;
    }
    compile(ctx: Ctx) {
        this.expr.compile(ctx);
        ctx.emit(Command.POP_TOP);
    }
}

export class Block extends AstNode {
    private readonly statements: Statement[];

    constructor(statements: Statement[]) {
        super();
        this.statements = statements;
    }
    compile(ctx: Ctx) {
        this.statements.forEach((s) => s.compile(ctx));
    }
}
