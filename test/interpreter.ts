import { Command } from './ast';

export class Interpreter {
    private readonly stack: any[] = [];
    private readonly bytecode: number[];
    private readonly consts: any[];

    constructor(bytecode: number[], consts: any[]) {
        this.bytecode = bytecode;
        this.consts = consts;
    }

    private push(val: any) {
        this.stack.push(val);
    }
    private pop() {
        return this.stack.pop();
    }

    public interpret() {
        let pc = 0;

        while (pc < this.bytecode.length) {
            const opcode = this.bytecode[pc];
            const opname = Command[opcode];
            pc = this[opname as keyof Interpreter](pc) as number;
        }
    }

    LOAD_CONST(pc: number) {
        const arg = this.bytecode[pc + 1];
        this.push(this.consts[arg]);
        return pc + 2;
    }
    BINARY_ADD(pc: number) {
        const a = this.pop();
        const b = this.pop();
        this.push(b + a);
        return pc + 1;
    }
    BINARY_MINUS(pc: number) {
        const a = this.pop();
        const b = this.pop();
        this.push(b - a);
        return pc + 1;
    }
    BINARY_MULTIPLY(pc: number) {
        const a = this.pop();
        const b = this.pop();
        this.push(b * a);
        return pc + 1;
    }
    BINARY_DIVIDE(pc: number) {
        const a = this.pop();
        const b = this.pop();
        this.push(b / a);
        return pc + 1;
    }
    POP_TOP(pc: number) {
        const value = this.pop();
        console.log(value);

        return pc + 1;
    }
}
